package models;

// Класс PlayerImpl с общей реализацией методов интерфейса Player
public class PlayerImpl implements Player {

    protected String name;
    protected Board board;

    public PlayerImpl(String name) {
        this.name = name;
        this.board = new Board();
    }

    @Override
    public void placeShips() {
        System.out.println(name + " Ваши корабли раставятся автоматически");
        board.placeShips();
    }

    public void placeShipsManual() {
        System.out.println(name + ", расставьте ваши корабли.");
        board.placeShipsManually();
    }

    @Override
    public void makeMove(Board opponentBoard) {
        System.out.println(name + ", ваш ход.");
        // Здесь оставим пустым, так как конкретная реализация будет в HumanPlayer и ComputerPlayer
    }

    @Override
    public boolean isDefeated() {
        return board.allShipsSunk();
    }

    public String getName() {
        return name;
    }

    public Board getBoard() {
        return board;
    }
}


