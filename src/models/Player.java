package models;

public interface Player {

    void placeShips();

    public void placeShipsManual();

    void makeMove(Board opponentBoard);

    boolean isDefeated();

    public String getName();

    public Board getBoard();

}

