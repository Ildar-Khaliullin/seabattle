package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

// Класс Board для представления игрового поля
public class Board {

    public static final int SIZE = 16; // Размер игрового поля (16x16)

    private char[][] grid; // Двумерный массив для хранения состояния поля

    private List<Ship> ships;

    public Board() {
        this.grid = new char[SIZE][SIZE];
        this.ships = new ArrayList<>();
        initializeGrid();
    }

    // Инициализация поля
    private void initializeGrid() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                grid[i][j] = '~'; // '~' - пустая клетка (вода)
            }
        }
    }

    //Ручная растановка кораблей
    public void placeShipsManually() {
        Scanner scanner = new Scanner(System.in);
        int[] shipSizes = {6, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1};

        for (int size : shipSizes) {
            boolean placed = false;
            while (!placed) {
                System.out.println("Введите координаты для корабля размером " + size + " (например, A1) и направление (H/V):");
                String input = scanner.nextLine();
                String[] parts = input.split(" ");
                if (parts.length != 2) {
                    System.out.println("Неправильный формат ввода. Попробуйте снова.");
                    continue;
                }

                String coord = parts[0].toUpperCase();
                String direction = parts[1].toUpperCase();
                int row = Integer.parseInt(coord.substring(1)) - 1;
                int col = coord.charAt(0) - 'A';
                boolean isHorizontal = direction.equals("H");

                if (canPlaceShip(row, col, size, isHorizontal)) {
                    for (int i = 0; i < size; i++) {
                        if (isHorizontal) {
                            grid[row][col + i] = 'S'; // 'S' - клетка с кораблем
                        } else {
                            grid[row + i][col] = 'S';
                        }
                    }
                try {
                    ships.add(new Ship(row, col, size, isHorizontal));
                    draw();
                    placed = true;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }

                } else System.out.println("Невозможно поставить корабль");

            }
        }
    }

    public void placeShips() {
        placeShip(6); // 1 корабль на 6 клеток
        placeShip(5); // 2 корабля на 5 клеток
        placeShip(5);
        placeShip(4); // 3 корабля на 4 клетки
        placeShip(4);
        placeShip(4);
        placeShip(3); // 4 корабля на 3 клетки
        placeShip(3);
        placeShip(3);
        placeShip(3);
        placeShip(2); // 5 кораблей на 2 клетки
        placeShip(2);
        placeShip(2);
        placeShip(2);
        placeShip(2);
        placeShip(1); // 6 кораблей на 1 клетку
        placeShip(1);
        placeShip(1);
        placeShip(1);
        placeShip(1);
        placeShip(1);
    }

    // авто расстановка кораблей на поле
    public void placeShip(int size) {
        Random random = new Random();
        boolean placed = false;

        while (!placed) {
            int row = random.nextInt(SIZE);
            int col = random.nextInt(SIZE);
            boolean horizontal = random.nextBoolean();

            if (canPlaceShip(row, col, size, horizontal)) {
                for (int i = 0; i < size; i++) {
                    if (horizontal) {
                        grid[row][col + i] = 'S'; // 'S' - клетка с кораблем
                    } else {
                        grid[row + i][col] = 'S';
                    }
                }
                ships.add(new Ship(row, col, size, horizontal));
                placed = true;
            }
        }
    }

    //проверка размещения корабля
    private boolean canPlaceShip(int row, int col, int size, boolean isHorizontal) {
        int dRow = isHorizontal ? 0 : 1;
        int dCol = isHorizontal ? 1 : 0;

        for (int i = 0; i < size; i++) {
            int newRow = row + i * dRow;
            int newCol = col + i * dCol;

            // Проверка выхода за границы поля
            if (newRow < 0 || newRow >= SIZE || newCol < 0 || newCol >= SIZE || grid[newRow][newCol] != '~') {
                return false;
            }

            // Проверка клеток вокруг корабля
            for (int dr = -1; dr <= 1; dr++) {
                for (int dc = -1; dc <= 1; dc++) {
                    int adjRow = newRow + dr;
                    int adjCol = newCol + dc;
                    if (adjRow >= 0 && adjRow < SIZE && adjCol >= 0 && adjCol < SIZE && grid[adjRow][adjCol] != '~') {
                        return false;
                    }
                }
            }
        }
        return true;

    }


    // Совершение хода игрока на поле
    public void makeMove(Board opponentBoard, String move) {
        int[] coordinates = parseMove(move);
        int row = coordinates[0];
        int col = coordinates[1];
        char result;

        if (isValidMove(row, col)) {
            result = opponentBoard.receiveAttack(row, col);

            if (result == 'X') {
                opponentBoard.grid[row][col] = 'X';
                System.out.println("Попадание");// 'X' - попадание
            }
            else if(result == '.') {
                opponentBoard.grid[row][col] = '.';
                System.out.println("Мимо");// '.' - мимо

            }

        } else {
            System.out.println("Неверные координаты хода.");

        }

    }


    // Получение результатов атаки на поле соперника
    private char receiveAttack(int row, int col) {
        if (grid[row][col] == 'S') {
            return 'X'; // Мимо
        } else {
            return '.'; // Попадание
        }
    }

    // Проверка на корректность координат хода
    private boolean isValidMove(int row, int col) {
        return row >= 0 && row < SIZE && col >= 0 && col < SIZE;
    }

    // Проверка поражения (все корабли потоплены)
    public boolean allShipsSunk() {
        return ships.isEmpty();// Все места с кораблями потоплены
    }

    // Парсинг введенного хода в координаты
    private int[] parseMove(String move) {
        int[] coordinates = new int[2];
        try {
            char colChar = move.charAt(0);
            int col = colChar - 'A';
            int row = Integer.parseInt(move.substring(1)) - 1;
            coordinates[0] = row;
            coordinates[1] = col;
        } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
            System.out.println("Ошибка парсинга координат хода.");
        }
        return coordinates;
    }

    // Отрисовка игрового поля
    public void draw() {
        System.out.print("  ");
        for (int i = 0; i < SIZE; i++) {
            System.out.print((char) ('A' + i) + " ");
        }
        System.out.println();

        for (int i = 0; i < SIZE; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < SIZE; j++) {
                System.out.print(grid[i][j] + " ");
            }
            System.out.println();
        }
    }
    //отрисовка для честной игры
    public void drawForFairPlay() {
        System.out.print("  ");
        for (int i = 0; i < SIZE; i++) {
            System.out.print((char) ('A' + i) + " ");
        }
        System.out.println();

        for (int i = 0; i < SIZE; i++) {
            System.out.print((i + 1) + (i + 1 < 10 ? " " : ""));
            for (int j = 0; j < SIZE; j++) {
                char cell = grid[i][j];
                if (cell == 'S') {
                    System.out.print("~ "); // Скрываем нетронутые корабли для противника
                } else {
                    System.out.print(cell + " ");
                }
            }
            System.out.println();
        }
    }
}

