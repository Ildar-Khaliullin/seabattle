package models;

// Класс ComputerPlayer, использующий PlayerImpl
public class ComputerPlayer extends PlayerImpl {

    public ComputerPlayer(String name) {
        super(name);
    }
    // ход компьютера
    @Override
    public void makeMove(Board opponentBoard) {
        super.makeMove(opponentBoard); // вызываем реализацию из PlayerImpl
        System.out.println(name + " делает ход.");

        String move = generateRandomMove();
        System.out.println(name + " выбирает координаты " + move);

        board.makeMove(opponentBoard, move);
    }

    private String generateRandomMove() {
            // Пример реализации случайного хода
            int row = (int) (Math.random() * Board.SIZE) + 1; // генерация случайной строки
            int col = (int) (Math.random() * Board.SIZE); // генерация случайной колонки (0 до 15)
            char colChar = (char) ('A' + col); // преобразование в буквенное обозначение колонки
            return colChar + "" + row;

    }
}

