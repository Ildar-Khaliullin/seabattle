package models;

import java.util.Scanner;

// Класс HumanPlayer, использующий PlayerImpl
public class HumanPlayer extends PlayerImpl {

    private Scanner scanner;

    public HumanPlayer(String name) {
        super(name);
        this.scanner = new Scanner(System.in);
    }

    //ход человека
    @Override
    public void makeMove(Board opponentBoard) {
        super.makeMove(opponentBoard); // вызываем реализацию из PlayerImpl
        System.out.println("Введите координаты для атаки (например, A1):");
        String move = scanner.nextLine().toUpperCase().trim();
        board.makeMove(opponentBoard, move);
    }

}
