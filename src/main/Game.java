package main;

import models.ComputerPlayer;
import models.HumanPlayer;
import models.Player;

import java.util.Scanner;

public class Game {

    private String player1Name;
    private String player2Name;
    private Player player1;
    private Player player2;
    private boolean singlePlayerMode;

    public Game(String player1Name, boolean singlePlayerMode) {
        this.player1Name = player1Name;
        this.singlePlayerMode = singlePlayerMode;
        if (singlePlayerMode) {
            this.player1 = new HumanPlayer(player1Name);
            this.player2Name = "Компьютер";
            this.player2 = new ComputerPlayer("Компьютер");
        }
    }

    public Game(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player1 = new HumanPlayer(player1Name);
        this.player2Name = player2Name;
        this.player2 = new HumanPlayer(player2Name);
    }

    public void start() {
        System.out.println("Игра начинается между игроком " + player1Name + " и игроком " + player2Name + ".");

        // Расстановка кораблей
        if (singlePlayerMode) {
            doPlaceShipsWithComputer();
        } else doPlaceShipsForHuman();
        // Основной игровой цикл
        while (!checkGameOver()) {
            // Ход игрока 1
            System.out.println("\nХод игрока " + player1Name);
            makeMove(player1, player2);
            System.out.println("Поле игрока " + player2Name );
            player2.getBoard().drawForFairPlay();
            //player2.getBoard().draw(); //можно использовать для отладки(отбражает фактическое состояние поля)

            // Проверка на конец игры после хода игрока 1
            if (checkGameOver()) break;

            // Ход игрока 2
            System.out.println("\nХод игрока " + player2Name);
            makeMove(player2, player1);
            System.out.println("Поле игрока " + player1Name);
            player1.getBoard().drawForFairPlay();
            //player1.getBoard().draw(); //можно использовать для отладки(отбражает фактическое состояние поля)
        }

        // Вывод результатов игры
        System.out.println("\nИгра окончена!");
        // Дополнительные действия по завершению игры (сохранение результатов, логирование и т.д.)
    }
    //Растановка кораблей для одиночной игры
    private void doPlaceShipsWithComputer(){
        System.out.println(player1Name + " Хотите расставить корабли автоматически?\n1 - Да\n2 - Нет");
        Scanner scanner = new Scanner(System.in);
        String choiceAutomaticOrManualP1 = scanner.nextLine();
        //Ручная растановка кораблей
        if (choiceAutomaticOrManualP1.equals("1")) {
            player1.placeShips();
        } else if (choiceAutomaticOrManualP1.equals("2")) {
            player1.placeShipsManual();
        } else {
            System.out.println("Не верный ввод");
        }
        player2.placeShips();

    }
    //Растановка кораблей для 2 человек
    private void doPlaceShipsForHuman() {
        System.out.println(player1Name + " Хотите расставить корабли автоматически?\n1 - Да\n2 - Нет");
        Scanner scanner = new Scanner(System.in);
        String choiceAutomaticOrManualP1 = scanner.nextLine();

        if (choiceAutomaticOrManualP1.equals("1")) {
            player1.placeShips();
        } else if (choiceAutomaticOrManualP1.equals("2")) {
            player1.placeShipsManual();
        }

        System.out.println(player2Name + " Хотите расставить корабли автоматически?\n1 - Да\n2 - Нет");
        Scanner scanner1 = new Scanner(System.in);
        String choiceAutomaticOrManualP2 = scanner1.nextLine();
        //Ручная растановка кораблей
        if (choiceAutomaticOrManualP2.equals("1")) {
            player2.placeShips();
        } else if (choiceAutomaticOrManualP2.equals("2")) {
            player2.placeShipsManual();
        }


    }

    private void makeMove(Player currentPlayer, Player opponentPlayer) {
        // Ход текущего игрока
        currentPlayer.makeMove(opponentPlayer.getBoard());
     }

    private boolean checkGameOver() {
        // Проверка условия окончания игры (в зависимости от логики игры)
        return player1.isDefeated() || player2.isDefeated();
    }
}

