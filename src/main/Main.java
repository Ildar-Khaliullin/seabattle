package main;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Добро пожаловать в игру Морской бой!");

        Scanner scanner = new Scanner(System.in);

        // Запрашиваем имя игрока
        System.out.print("Введите ваше имя: ");
        String playerName = scanner.nextLine();

        // Запрашиваем выбор режима игры
        System.out.println("Выберите режим игры:");
        System.out.println("1. Одиночная игра с ботом");
        System.out.println("2. Игра с другим игроком");

        int choice = scanner.nextInt();
        scanner.nextLine(); // считываем лишний перевод строки

        // Создаем экземпляр игры в зависимости от выбора пользователя
        Game game = null;
        if (choice == 1) {
            game = new Game(playerName, true); // Одиночная игра с ботом
        } else if (choice == 2) {
            // Запрашиваем имя второго игрока
            System.out.print("Введите имя второго игрока: ");
            String playerName2 = scanner.nextLine();
            game = new Game(playerName, playerName2); // Игра с другим игроком
        } else {
            System.out.println("Некорректный выбор. Завершение программы.");
            return;
        }

        // Запускаем игру
        game.start();

    }
}

